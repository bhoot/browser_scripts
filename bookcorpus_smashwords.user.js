// ==UserScript==
// @name         Bookcorpus Make
// @namespace    http://adnanyun.us/
// @version      0.1
// @description  Download all the books!
// @author       Adnan Yunus
// @match        *://www.smashwords.com/*
// @grant        GM_notification
// @run-at       document-end

// ==/UserScript==
const delay = 500;

function Sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

function addDashboard() {
    const running = localStorage.getItem('bcRunning') === "true";
	const bcBtn = document.createElement('div');
	bcBtn.style = 'position: absolute;right: 0px;top: 35px;padding: 5px;cursor: pointer;color: rgb(152, 170, 150);background-color: black;border-radius: 5%; z-index: 999;';
    bcBtn.id = 'bc';
	bcBtn.innerText = running ? 'Stop Bookcorpus': 'Run Bookcorpus';
	bcBtn.onclick = () => {
        localStorage.setItem('bcRunning', !running);
        location.reload();
    };
	document.body.appendChild(bcBtn);
	return running;
}

function downloadAsJson(val, fileName) {
    const a = window.document.createElement('a');
    a.href = window.URL.createObjectURL(new Blob([JSON.stringify(val, null, 2)], {type: 'application/json'}));
    a.download = fileName;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);

}
async function start() {

    const running = addDashboard();

    if (running) {
        const pageLinks = document.querySelectorAll('.page-link');

        if (!pageLinks.length) {
            const cartBtn = document.querySelector('[title="Add this ebook to your shopping cart"]');
            const checkoutBtn = document.querySelector('[value="Checkout"]');
            const priceInput = document.querySelector('#price');
            const savedPage = localStorage.getItem('savedPage');
            const txtDownload = document.querySelector('[title="Archival; contains no formatting"]');
            const epubDownload = document.querySelector('[title="Nook, Kobo, Sony Reader, and tablets"]');
            const downloaded = localStorage.getItem(location.pathname);
            const checkoutDone = location.pathname.match('checkout/complete');
            console.log(cartBtn, checkoutBtn, priceInput, savedPage, downloaded);

            if (cartBtn && cartBtn.innerText.trim() === 'Buy') {
                await Sleep(delay);
                cartBtn.click();
            } else if (checkoutBtn) {
                await Sleep(delay);
                checkoutBtn.click();
            } else if (priceInput) {
                priceInput.value = 0;
                await Sleep(delay);

                document.querySelector('[value="Go"]').click();
            } else if (!downloaded && !checkoutDone) {
                async function setDownloaded(link) {
                    const fileNameSplit = link.href.split('/');
                    const fileName = fileNameSplit[fileNameSplit.length -1];
                    const name = document.querySelector('[itemprop="name"]');
                    const badges = document.querySelectorAll('.badge');
                    const author = document.querySelector('[itemprop="author"]');
                    const category = document.querySelector('[itemprop="genre"]');
                    const date = document.querySelector('[itemprop="datePublished"]');
                    const words = date.nextElementSibling;
                    const language = words.nextElementSibling;
                    const isbn = document.querySelector('[itemprop="isbn"]');
                    const keywords = document.querySelectorAll('[itemprop="keywords"] a');
                    const info = {
                        name: name ? name.innerText : '',
                        badges: Object.keys(badges).map(k => badges[k].innerText),
                        author: author ? author.innerText : '',
                        category: category ? category.innerText.split(':')[1].trim() : '',
                        date: date ? date.innerText.split(':')[1].trim() : '',
                        words: words ? words.innerText.split(':')[1].trim() : '',
                        language: language ? language.innerText.split(':')[1].trim() : '',
                        isbn: isbn ? isbn.innerText : '',
                        keywords: Object.keys(keywords).map(k => keywords[k].innerText),
                        url: link.pathname,
                        page: location.pathname,
                        fileName
                    };

                    downloadAsJson(info, fileName.split('.')[0] + '.json');

                    await Sleep(delay);
                    link.click();

                    localStorage.setItem(location.pathname, fileName);
                    await Sleep(delay);
                    document.location.href = savedPage;
                }
                if (txtDownload) {
                    await setDownloaded(txtDownload);
                } else if (epubDownload) {
                    await setDownloaded(epubDownload);
                } else {
                    const e = 'no valid format found, stalled, trying default';
                    const dlList = document.querySelector('#download');
                    console.log(e);
                    const bcBtn = document.querySelector('#bc');
                    if (bcBtn) {
                        bcBtn.style.color = 'red';
                        bcBtn.innerText = e;
                    }
                    if (dlList) {
                        const defaultDownload = dlList.querySelector('[title]');
                        if (defaultDownload) {
                            await setDownloaded(defaultDownload);
                        } else {
                             await Sleep(delay);
                             localStorage.setItem(location.pathname, 'not downloaded');
                             location.href = savedPage;
                         }
                    }
                }
            } else if (savedPage) {
                if (checkoutDone) {
                    let lastBook = parseInt(localStorage.getItem('lastBook'));
                    if (lastBook) {
                        localStorage.setItem('lastBook', lastBook - 1);
                    } else if (lastBook == 0) {
                        localStorage.removeItem('lastBook');
                    }
                }
                await Sleep(delay);
                document.location.href = savedPage;
            } else {
                const e = 'no saved page found and nothing to do';
                console.log(e);
                const bcBtn = document.querySelector('#bc');
                if (bcBtn) {
                    bcBtn.style.color = 'red';
                    bcBtn.innerText = e;
                }
            }

        } else {
            localStorage.setItem('savedPage', document.location.href);

            let lastBook = parseInt(localStorage.getItem('lastBook'));
            const books = document.querySelectorAll('.library-book');
            if (books.length) {
//                console.log(lastBook, pageLinks);

                if (!lastBook && lastBook != 0) {
                    lastBook = 0;
                } else {
                    lastBook += 1;
                    localStorage.setItem('lastBook', lastBook);
                }
                //console.log(lastBook);

                if (lastBook < books.length) {
                    const title = books[lastBook].querySelector('.library-title');
                    console.log(lastBook, title.innerText, title.pathname, localStorage.getItem(title.pathname));
                    await Sleep(delay);

                    localStorage.setItem('lastBook', lastBook);
                    title.click();
                } else {
                    console.log('reached last one');
                    localStorage.setItem('lastBook', 0);

                    await Sleep(delay);
                    pageLinks[0].click();
                }

            }

        }
    }
}
(function() {
    'use strict';
    start();
})();
