// ==UserScript==
// @name         US Maps
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  Convenience script to make it easy to download data from US Maps
// @author       Adnan Yunus
// @match        http*://viewer.nationalmap.gov/*
// @include      http*://viewer.nationalmap.gov*
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_listValues
// @downloadURL  https://gitlab.com/bhoot/browser_scripts/raw/master/us_maps.user.js
// @updateURL    https://gitlab.com/bhoot/browser_scripts/raw/master/us_maps.user.js
// @run-at       document-end
// ==/UserScript==
let handle;
(function() {
    'use strict';
    function baseName(s) {
        let base = s.substring(s.lastIndexOf('/') + 1);
        if(base.lastIndexOf(".") != -1) {
            base = base.substring(0, base.lastIndexOf("."));
        }
        return base;
    }

    function callback() {
        if (handle) {
            clearTimeout(handle);
        }
        handle = setTimeout(function() {

            Array.from(document.querySelectorAll('[title="Download the file."]')).forEach(e => {
                const h = baseName(e.href);
                const x = GM_getValue(h);
                if (!x) {
                    GM_setValue(h, e.href);
                }
            });
            const current_nav = document.querySelector('.goToPage.selected');
            const navs = Array.from(document.querySelectorAll('.goToPage:not(.selected)'));
            if (current_nav) {
                let n = navs.find(e => parseInt(e.id) > parseInt(current_nav.id));
                if (!n) {
                    n = document.querySelector('.goToNext');
                }
                const vals = GM_listValues();
                console.log(vals.length, current_nav.id, n);
                n.click();
            }
        }, 2000);
    }

    const targetNode = document.querySelector('body');
    // Create an observer instance linked to the callback function
    const observer = new MutationObserver(callback);

    // Start observing the target node for configured mutations
    observer.observe(targetNode, {
        childList: true,
        subtree: true,
        attributes: true
    });

})();
