// ==UserScript==
// @name         Twitter Cleanup
// @namespace    http://adnanyun.us/
// @version      0.7
// @description  Cleans up twitter user interface
// @author       Adnan Yunus
// @match        http*://twitter.com/*
// @include      http*://twitter.com*
// @grant        GM_setClipboard
// @grant        GM_addStyle
// @downloadURL  https://gitlab.com/bhoot/browser_scripts/raw/master/twitter_cleanup.user.js
// @updateURL    https://gitlab.com/bhoot/browser_scripts/raw/master/twitter_cleanup.user.js
// @run-at       document-end
// ==/UserScript==

GM_addStyle ( `
    .first-load.mute-button {
        display: inherit !important;
    }
    .mute-user-item {
        width: fit-content;
        padding: 3px 5px;
        background-color: #ff9800;
        color: white;
        font-weight: 700;
        justify-content: center;
        align-items: center;
        box-sizing: border-box;
        display: flex;
        flex-direction: row;
        cursor: pointer;
        margin-top: 5px;
        margin-bottom: 5px;
        border-radius: 5px;
        box-shadow: 1px 1px 4px rgba(0, 0, 0, 0.4);
    }
    .mute-user-item:hover {
        background-color: #e08600;
    }
` );


function closeDialog() {
    const dialog = document.querySelector('[aria-controls=embed-tweet-dialog-dialog]');
    if (dialog) {
        dialog.click();
    }
}

function copyEmbedInfo(n) {
    if (n) {
        const url = n.match(/href="([^ ]+)">pic.twitter.com[^@]+(@[^)]+)/);
        if (url) {
            GM_setClipboard(`\nvia ${url[2]}\n\n${url[1]}`);
            closeDialog();
        }
    }
}

function addMuteButton(n) {
    //let t = n.querySelector('.stream-item-header .time');
    const c = n.querySelector('.content');
    const m = n.querySelector('.mute-user-item');
    if (m && c) {
        c.insertBefore(m, null);
    }
}

(function() {
    'use strict';

    var divs = document.querySelectorAll('li.js-stream-item'), i;

    for (i = 0; i < divs.length; ++i) {
        addMuteButton(divs[i]);
    }

    const callback = function(mutationsList, observer) {
        for(let mutation of mutationsList) {
            if (mutation.type === 'childList') {
                for (const n of mutation.addedNodes) {
                    if (n.querySelector) {
                        //console.log(mutation);
                        addMuteButton(n);
                    }
                }
            }
            else if (mutation.type === 'attributes') {
                const embed_text = mutation.target.querySelector('textarea.embed-destination');
                //copyEmbedInfo();
                if (embed_text && embed_text.value) {
                    //console.log(mutation.target, embed_text.value);
                    copyEmbedInfo(embed_text.value);

                }

            }
        }
    };


    const targetNode = document.querySelector('body');
    // Create an observer instance linked to the callback function
    const observer = new MutationObserver(callback);

    // Start observing the target node for configured mutations
    observer.observe(targetNode, {
        childList: true,
        subtree: true,
        attributes: true
    });


})();
