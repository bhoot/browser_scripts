// ==UserScript==
// @name         Llama Philanthropist
// @namespace    http://adnanyun.us/
// @version      0.20
// @description  Give Teh Lamas to everyone!
// @author       Udnaan
// @match        *://*.deviantart.com/*
// @downloadURL  https://gitlab.com/bhoot/browser_scripts/raw/master/llama_philanthropist.user.js
// @updateURL    https://gitlab.com/bhoot/browser_scripts/raw/master/llama_philanthropist.user.js
// @grant GM_notification
// @run-at          document-end
// ==/UserScript==

let dashboard = null;
function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}
function addDashboard() {
	const llama_giver = document.createElement('div');
	llama_giver.style = 'position: absolute;right: 0px;top: 35px;padding: 5px;cursor: pointer;color: rgb(152, 170, 150);background-color: black;border-radius: 5%; z-index: 999;';
	llama_giver.innerText = 'Give Llama';
	llama_giver.onclick = giveLlamaToAll;
	document.body.appendChild(llama_giver);
	return llama_giver;
}
async function giveLlamaToAll() {
	dashboard.innerText = 'Stop Giving';
	await sleep(3000);
	while (true) {
		const main_page = document.location.hostname === 'www.devaintart.com';
		const llama = document.getElementsByClassName('oclb-give')[0];
		const spam = document.getElementsByClassName('oclb-spam')[0];
		if (!llama && !spam) {
			const next = document.querySelector('.comments-footer-container .browse-proper-button') || document.querySelector('.next .away') || document.querySelector('.load_more');
			const text = `Llamas given to everyone! ${next ? 'continuing' : 'stopping'}`;

			console.log(text);
			GM_notification({
				text,
				title: 'Llama Philanthropist',
				timeout: 10000,
				onclick: function() { window.focus(); },
			});
			if (next) {
				await sleep(2000);
				next.click();
				await sleep(2000);
			} else {
				break;
			}
			if (main_page) window.scrollTo(0, document.body.scrollHeight);
		} else if (spam) {
			const text = 'spam watch triggered. Waiting for 10 minutes';
			console.log(text);
			GM_notification({
				text,
				title: 'Llama Philanthropist',
				timeout: 10000,
				onclick: function() { window.focus(); },
			});
			await sleep(300000);
		} else {
			const ev = new Event('click');
			llama.onclick(ev)
			let p = llama.closest('.minipage-info-container');
			if (!p) p = llama.closest('.ccomment');
			let image = '';
			if (p) image = p.querySelector('a img').src;
			const text = 'given llama to ' + llama.parentNode.innerText;
			console.log(text, image);
			GM_notification({
				text,
				image,
				title: 'Llama Philanthropist',
				timeout: 6000,
				onclick: function() { window.focus(); },
			});
			await sleep(10000);
		}
	}
	dashboard.innerText = 'Give Llama';
}
(function() {
	'use strict';
	dashboard = addDashboard();
})();
