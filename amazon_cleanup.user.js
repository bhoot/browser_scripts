// ==UserScript==
// @name         Amazon Cleanup
// @namespace    http://adnanyun.us/
// @version      0.2
// @description  Cleans up Amazon broken links
// @author       Adnan Yunus
// @match        http*://amazon.com/*
// @grant        none
// @include      *://amazon.com
// @include      *://*.amazon.com
// @include      *://*.amazon.*/*
// @include      *://amazon.*/*
// @downloadURL  https://gitlab.com/bhoot/browser_scripts/raw/master/amazon_cleanup.user.js
// @updateURL    https://gitlab.com/bhoot/browser_scripts/raw/master/amazon_cleanup.user.js
// ==/UserScript==

document.body.onclick = function(e) {
    const el = e.target;
    if (el.href && el.href.match('gp/offer-listing')) {
        window.location.assign(el.href);
    }
}
