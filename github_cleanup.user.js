// ==UserScript==
// @name         Github Cleanup
// @namespace    http://adnanyun.us/
// @version      0.1
// @description  Cleans up github broken links
// @author       Adnan Yunus
// @match        http*://github.com/*
// @grant        none
// @include      http*://github.com*
// @downloadURL  https://gitlab.com/bhoot/browser_scripts/raw/master/github_cleanup.user.js
// @updateURL    https://gitlab.com/bhoot/browser_scripts/raw/master/github_cleanup.user.js
// @run-at       document-end
// ==/UserScript==

function replaceExploreWithTrending(node) {
	Array.from(document.getElementsByTagName('a')).filter(n => n.href === location.origin + '/explore')[0].href = '/trending';
}

replaceExploreWithTrending(document.body);
