// ==UserScript==
// @name         Deviant Art Notifications
// @namespace    http://adnanyun.us/
// @version      0.1
// @description  Reply, comment!
// @author       Udnaan
// @match        *://www.deviantart.com/notifications/*
// @downloadURL  https://gitlab.com/bhoot/browser_scripts/raw/master/da_notif.user.js
// @updateURL    https://gitlab.com/bhoot/browser_scripts/raw/master/da_notif.user.js
// @grant GM_notification
// @run-at          document-end
// ==/UserScript==

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}
async function replyCommentToAll() {
}

(function() {
	'use strict';
	replyCommentToAll();
})();
