// ==UserScript==
// @name         Objectivist
// @namespace    https://adnanyun.us/
// @version      0.3
// @description  Translate latent langauge into accurate meaning
// @author       Adnan Yunus
// @downloadURL  https://gitlab.com/bhoot/browser_scripts/raw/master/objectivist_translater.user.js
// @updateURL    https://gitlab.com/bhoot/browser_scripts/raw/master/objectivist_translater.user.js
// @match        http://*/*
// @include      *://*/*
// @grant        none
// @run-at       document-idle
// ==/UserScript==

(function() {
    'use strict';

    const translate = {
        "helpful": "scammy",
        "covid": "global-scam",
        "coaching": "scamming",
        "coach": "scammer",
        "essential": "bum",
        "nurse": "clueless"

    };
    const replaceKeys = Object.keys(translate).map(function(k) {
        return k.replace(/([-[\]{}()*+?.\\^$|#,])/g,'\\$1');
    });
    const xpathReplaceKeys = Object.keys(translate).map(function(k) { return `contains(translate(.,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'), '${k}')`; }).join(' or ');
    const regx = new RegExp(replaceKeys.join('|'), 'gi');
    const f = function(ele) {
        try {

            const candidates = document.evaluate(`//*[${xpathReplaceKeys}]`, document, null, XPathResult.ANY_TYPE, null );
            let candidate = candidates.iterateNext();
            while (candidate) {
                if (!candidate.children || !candidate.children.length || candidate.innerHTML.length < 500) {
                    console.log(candidate);
                    candidate.textContent = candidate.textContent.replace(regx, function(matched) {
                        console.log(matched);
                        return translate[matched.toLowerCase()];
                    });
                }
                candidate = candidates.iterateNext();
            }
        } catch(e) {}
    }

    const targetNode = document.querySelector('body');
    f(document.body);
    // Create an observer instance linked to the callback function

    const interval = setInterval(function() {
        console.log('running');
        f(document.body);

    }, 5000);
})();
