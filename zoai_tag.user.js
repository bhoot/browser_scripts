// ==UserScript==
// @name         Artometa ZoAI Paint
// @namespace    http://adnanyun.us/
// @version      0.36
// @description  Tag picture for zoai to paint!
// @author       Udnaan
// @match        *://*.instagram.com/*
// @require      https://cdnjs.cloudflare.com/ajax/libs/inferno/5.3.0/inferno.min.js
// @downloadURL  https://gitlab.com/bhoot/browser_scripts/raw/master/zoai_tag.user.js
// @updateURL    https://gitlab.com/bhoot/browser_scripts/raw/master/zoai_tag.user.js
// @run-at          document-end
// ==/UserScript==
(function() {
    'use strict';

    const postData = (url = ``, data = {}, method = "POST") => {
        return fetch(url, {
            method, // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(data), // body data type must match "Content-Type" header
        })
            .then(response => response.json()) // parses response to JSON
            .catch(error => console.error(`Fetch Error =\n`, error));
    }

    function queueImage(item, e) {
        // console.log(e.target, item, item.selector.value);
        e.target.style.color = 'grey';
        const { image, owner } = item;
        postData(`https://artometa-web.firebaseio.com/stylize_paint.json`, { image, owner, style: item.selector.value, module: 'ig'})
            .then(data => {
            e.target.style.backgroundColor = 'green';
            e.target.style.color = 'white';
           // console.log(data);
        })
            .catch(error => console.error(error));
    }

        const dialog = `<dialog id="settingsDialog">
<form method="dialog" id="updateSettings">
<section>
<p><label for="caption">Caption:</label><br/>
<textarea id="caption" cols="50" rows="8" wrap="soft"></textarea>
</p>
<p><label for="overlay">Overlay:</label><br/>
<textarea id="overlay" cols="35" rows="3" wrap="soft"></textarea>
</p>
</section>
<menu>
<button id="cancelDialog" type="reset">Cancel</button>
<button type="submit">Update</button>
</menu>
</form>
</dialog>`;
    function getAvailSettings() {
        return Object.values(document.querySelector('form#updateSettings').elements).map(e => ({tag: e.tagName, name: e.id, value: e.value})).filter(e => e.tag !== 'BUTTON');
    }
    function updateSettings(e) {
        e.preventDefault();
        let req = {};
        for (let setting of getAvailSettings()) {
            req[setting.name] = setting.value;
        }
        // console.log(req);
        postData(`https://artometa-web.firebaseio.com/settings/ig.json`, req, "PATCH")
            .then(data => {
            // console.log(data);
        }).catch(error => console.error(error));
        this.submit();
    }
    function openDialog(dialog) {
        fetch('https://artometa-web.firebaseio.com/settings/ig.json')
            .then(function(response) {
            return response.json()
        }).then(function(ig) {
            //console.log(ig, document.forms.updateSettings.elements.caption.value);
            for (let setting of getAvailSettings()) {
                document.querySelector(`form#updateSettings #${setting.name}`).value = ig[setting.name] || '';
            }
            dialog.showModal()
        });
    }
    function createNav() {
        const navItem = document.querySelector('nav .XrOey');
        if (!document.querySelector('nav .artometa') && navItem) {
            document.body.insertAdjacentHTML( 'beforeend', dialog);
            const setting = navItem.cloneNode(true);
            setting.firstChild.className = 'glyphsSpriteSettings__outline__24__grey_9 artometa';
            setting.firstChild.innerText = '';
            setting.firstChild.href = "javascript:void(0)";
            const settingsDialog = document.getElementById('settingsDialog');
            setting.firstChild.onclick = openDialog.bind(null, settingsDialog);
            settingsDialog.querySelector('#cancelDialog').onclick = () => settingsDialog.close();
            document.forms.updateSettings.addEventListener("submit", updateSettings);
            navItem.parentElement.prepend(setting);
        }
    }
    function createControls(parent, styles, options) {
        const style_list = Object.values(styles);
        const selector = document.createElement('select');
        selector.style.padding = '5px';
        for (let i = 0; i < style_list.length; i++) {
            const style = style_list[i];
            const option = document.createElement('option');
            option.value = style;
            option.text = style;
            selector.appendChild(option);
        }
        //console.log(selector)
        parent.appendChild(selector);

        const btn = document.createElement('div');
        btn.style.cursor = 'pointer';
        btn.style.padding = '5px';
        btn.innerText = 'Stylize';
        btn.className = 'artometa';
        btn.onclick = queueImage.bind(null, {...options, selector});
        parent.appendChild(btn);

    }
    function addBtn(styles) {
        createNav();
        const articles = document.querySelectorAll('article');
        for(let i = 0; i < articles.length; i++) {
            if(articles[i].children.length < 2 ||
              articles[i].children[0].querySelector('.artometa') ||
              !articles[i].children[1].querySelector('img') ||
              !articles[i].children[1].querySelector('img').src) {
                continue;
            }
            const image = articles[i].children[1].querySelector('img').src;
            const owner = articles[i].children[0].querySelector('a').pathname.split('/').join('');
            createControls(articles[i].children[0], styles, {image, owner});
        }
    }
    let stylized_list = {};
    function htmlToElement(html) {
        var template = document.createElement('template');
        html = html.trim(); // Never return a text node of whitespace as the result
        template.innerHTML = html;
        return template.content.firstChild;
    }
    function copyToClipboard(item, e){
        if (e.target.className.split(' ').indexOf('remove') > -1) {
            e.preventDefault();
            postData(`https://artometa-web.firebaseio.com/stylized/${item.key}.json`, {done: true}, 'PATCH')
                .then(r => console.log(r))
                .catch(err => console.log(err));
            document.querySelector(`[href="${item.processed_url}"]`).parentElement.remove();
        } else {
            var dummy = document.createElement("textarea");
            document.body.appendChild(dummy);
            dummy.value = item.caption;
            dummy.select();
            document.execCommand("copy");
            document.body.removeChild(dummy);
            Object.assign(document.createElement('a'), { target: '_blank', href: item.processed_url}).click();
        }
    }
    function updateImageView(items) {
        const sideItem = document.querySelector('main section').children[2].children[3];
        const imageList = document.querySelector('main section .imagelist');
        if (imageList) {
            imageList.remove();
        }
        if (sideItem) {
            let newImageList = sideItem.cloneNode(true);
            newImageList.className = newImageList.className + ' imagelist';
            newImageList.firstChild.firstChild.innerHTML = '';
            // console.log(newImageList);
            for (let item of items.reverse()) {
                const h = `<div style="height: 90px;">
<div class="BI5t6" style="cursor: pointer;">
<div class="RR-M-">
<span class="_2dbep" style="width: 70px; height: 70px;">
<img class="_6q-tv" src="${item.processed_url}">
</span></div>
<div class="c6Ldk"><div class="rdlLb"><span class="jQgLo">${item.caption}</span></div>
<div class="_8Scg1"><div class="ijlbM Nzb55 remove">remove</div></div>
</div></div></div>`;
                const htmlItem = htmlToElement(h);
                htmlItem.onclick = copyToClipboard.bind(null, item);
                newImageList.firstChild.firstChild.append(htmlItem);
            }
            sideItem.parentElement.append(newImageList);
        }
    }
    function wasUpdated(e) {
        //console.log("message: " + e.data);
        stylized_list = {...stylized_list, ...e.data};
        const items = Object.keys(stylized_list).filter(key => stylized_list[key].processed_url && !stylized_list[key].done).map(key => ({...stylized_list[key], key}));
        //console.log(items);
        updateImageView(items);
    }
    function updateWhenAvailable() {
        fetch('https://artometa-web.firebaseio.com/stylized.json')
        .then(function(response) {
            return response.json();
        }).then(function(stylized) {
            stylized_list = stylized;
            wasUpdated({e: {data: {}}});
            const evtSource = new EventSource("https://artometa-web.firebaseio.com/stylized.json");
            evtSource.onmessage = wasUpdated;
        });
    }

    fetch('https://artometa-web.firebaseio.com/settings/styles.json')
        .then(function(response) {
        return response.json();
    })
        .then(function(styles) {

        const observer = new MutationObserver(addBtn.bind(null, styles));
        const config = {
            'childList': true,
            'subtree': true
        };
        observer.observe(document.body, config);
        updateWhenAvailable();
    });
})();
